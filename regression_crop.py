# Import the packages and classes needed in this example:
import numpy as np
from sklearn.linear_model import LinearRegression

def regression(code,parameters,df,FAOitem,col_years):


    def make_valid_fao_year(year):
        """Make a valid fao year string from int year"""
        return "Y" + str(year)
    #print(parameters)
    #FAOitem=[221]

    for item in FAOitem:
        if not code in parameters.get("exeptions"):
            if not item in parameters.get("exeptions"):
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]
                backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1)))]

                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :
                    print(code)
                    for years in relevant_years:
                        #print("looking for first",years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            #print("first",first_year, code, item)
                            break                     

                    for years in backward:
                        #print("looking for last",years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            #print("last",last_year, code, item)

                            break
                        

                    if  df[col_years][((df['Item Code']==item)&(df['ISO3']==code))].isnull().values.all():
                        print(code, item, years,"tout est vide")
                        break
                       
                    
                    if first_year == last_year:
                        #print("unique valeur")
                        value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[make_valid_fao_year(first_year)]]
                        value=float(value.to_string(index=False, header=False))
                        for years in ([make_valid_fao_year(year) for year in list(range(parameters.get("year_of_interest").get("begin"),parameters.get("year_of_interest").get("end")+1))]):   
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = value
                        break
                   
                  
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] 
                            value=float(value.to_string(index=False, header=False))
                            #print(value, item, code)
                            year_zero = int(years.replace("Y",""))
                            if value == 0 :
                                for years in ([make_valid_fao_year(year) for year in list(range(year_zero,parameters.get("year_of_interest").get("end")+1))]):   
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = 0
                                break
                            break
                        break
                            
                    if first_year == parameters.get("year_of_interest").get("begin") and last_year == parameters.get("year_of_interest").get("end"):
                        pass
                    
                    
                    
                    if last_year-first_year==1 :
                        valeur=0
                        
                        for years in range (first_year, last_year+1):
                            instant=df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]
                            instant=float(instant.to_string(index=False, header=False))
                            valeur=valeur+instant
                            #print(code, item,valeur, years)
                        valeur= valeur // 2
                        
                        
                        for years in range (parameters.get("year_of_interest").get("begin"),first_year):
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=valeur
                        for years in range (last_year,parameters.get("year_of_interest").get("end")+1):
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=valeur
                        df.to_csv('crop_regression.csv',index = False) 
                        break
                    if last_year-first_year<5 and last_year-first_year>=2:
                        
                        for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                            value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                            value1=float(value1.to_string(index=False, header=False))
                            value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                            value2=float(value2.to_string(index=False, header=False))
                            value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                            value3=float(value3.to_string(index=False, header=False))
                            average=(value1+value2+value3)/3
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                        df.to_csv('crop_regression.csv',index = False)    
                        for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                            value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                            value1=float(value1.to_string(index=False, header=False))
                            value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                            value2=float(value2.to_string(index=False, header=False))
                            value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                            value3=float(value3.to_string(index=False, header=False))
                            average=(value1+value2+value3)/3
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                        df.to_csv('crop_regression.csv',index = False)      
                        break
                    
                    if not first_year == parameters.get("year_of_interest").get("begin") and last_year == parameters.get("year_of_interest").get("end") and (last_year-first_year)>=5 :
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            print(first_year, last_year, code, item)
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("end"))]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            #print(np.sign(model.coef_),np.sign(model2.coef_))
                            if  np.sign(model.coef_) == np.sign(model2.coef_)  :
                                #print("meme signe, OK", code, item)
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, parameters.get("year_of_interest").get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                df.to_csv('crop_regression.csv',index = False)         
                                
                          
                            else : 
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('crop_regression.csv',index = False)            

                    
                    if  first_year == parameters.get("year_of_interest").get("begin") and not last_year == parameters.get("year_of_interest").get("end") and (last_year-first_year)>=5:
                        print("peut etre ici",code, item)
                        print("first year", first_year,"last year", last_year)

                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("year_of_interest").get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)

                            if  np.sign(model.coef_) == np.sign(model2.coef_) :

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("year_of_interest").get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([parameters.get("year_of_interest").get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                
                                df.to_csv('crop_regression.csv',index = False)         
                                
                            
                                
                            else :
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('crop_regression.csv',index = False)            
  
                                                                            
                    
                    if not first_year == parameters.get("year_of_interest").get("begin") and not last_year == parameters.get("year_of_interest").get("end") and (last_year-first_year)>=5:
                        print("first year", first_year,"last year", last_year)
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_) :
                             
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                df.to_csv('crop_regression.csv',index = False) 
                                
                                
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value         
                                df.to_csv('crop_regression.csv',index = False) 
                                
                            
                              
                                
                            else:
                                for years in range(first_year-1,parameters.get("year_of_interest").get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('crop_regression.csv',index = False) 
                                
                                df.to_csv('crop_regression.csv',index = False)
                                for years in range(last_year+1,parameters.get("year_of_interest").get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('crop_regression.csv',index = False) 

                                             
              
        if  code in parameters.get("exeptions"):
            if not item in parameters.get("exeptions"):
                relevant_years = [make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]
                backward = [make_valid_fao_year(year) for year in list(reversed(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1)))]
                if item in (df.loc[df['ISO3']==code, ["Item Code"]].values) :

                    for years in relevant_years:
                        #print("looking for first",years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            first_year = int(years.replace("Y",""))
                            print("first",first_year, code, item)
                            break                     
                   
                    for years in backward:
                        #print("looking for last",years)
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            last_year = int(years.replace("Y",""))
                            print("last",last_year, code, item)

                            break
                    if  df[col_years][((df['Item Code']==item)&(df['ISO3']==code))].isnull().values.all():
                        print(code, item, years,"tout est vide")
                        break
                        
                    if first_year == last_year:
                        #print("unique valeur")
                        value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[make_valid_fao_year(first_year)]]
                        value=float(value.to_string(index=False, header=False))
                        for years in ([make_valid_fao_year(year) for year in list(range(parameters.get("exeptions").get(code).get("begin"),parameters.get("exeptions").get(code).get("end")+1))]):   
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = value
                        break
                    
                    if first_year == parameters.get("exeptions").get(code).get("begin") and last_year == parameters.get("exeptions").get(code).get("end"):
                        continue
                    
                    if last_year-first_year==1 :
                        valeur=0
                        
                        for years in range (first_year, last_year+1):
                            instant=df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]
                            instant=float(instant.to_string(index=False, header=False))
                            valeur=valeur+instant
                            #print(code, item,valeur, years)
                        valeur= valeur // 2
                        
                        
                        for years in range (parameters.get("exeptions").get(code).get("begin"),first_year):
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=valeur
                        for years in range (last_year,parameters.get("exeptions").get(code).get("end")+1):
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=valeur
                        df.to_csv('crop_regression.csv',index = False) 
                    
                    
                        
                    if last_year-first_year<5 and last_year-first_year>=2:
                        
                        for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                            value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                            value1=float(value1.to_string(index=False, header=False))
                            value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                            value2=float(value2.to_string(index=False, header=False))
                            value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                            value3=float(value3.to_string(index=False, header=False))
                            average=(value1+value2+value3)/3
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                        df.to_csv('crop_regression.csv',index = False)    
                        for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                            value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                            value1=float(value1.to_string(index=False, header=False))
                            value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                            value2=float(value2.to_string(index=False, header=False))
                            value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                            value3=float(value3.to_string(index=False, header=False))
                            average=(value1+value2+value3)/3
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                        df.to_csv('crop_regression.csv',index = False)      
                    
                    
                    '''
                    if last_year-first_year<5 :
                        valeur=0
                        #df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                        #print(df[[make_valid_fao_year(year) for year in list(range(first_year,last_year+1))]])
                        for years in range (first_year, last_year+1):
                            instant=df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]
                            instant=float(instant.to_string(index=False, header=False))
                            valeur=valeur+instant
                            print(code, item,valeur, years)
                            
                        valeur= valeur // (last_year-first_year+1)
                        print(code, item, valeur)
                        for years in range (last_year+1,parameters.get("year_of_interest").get("end")+1 ):
                            print(code, item, years, valeur)
                            df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=valeur
                        break

                    '''                                    
                    for years in backward:
                        if  not df.loc[((df['Item Code']==item)&(df['ISO3']==code)),[years]].isnull().values.all():
                            value =df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] 
                            value=float(value.to_string(index=False, header=False))
                            year_zero = int(years.replace("Y",""))
                            if value == 0 :
                                for years in ([make_valid_fao_year(year) for year in list(range(year_zero,parameters.get("year_of_interest").get("end")+1))]):   
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),[years]] = 0
                                    print(code, item)
                                    
                    if not first_year == parameters.get("exeptions").get(code).get("begin") and last_year == parameters.get("exeptions").get(code).get("end") and (last_year-first_year)>=5:
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_) :

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end"))]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("end")-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, parameters.get("exeptions").get(code).get("end")-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                   
                                df.to_csv('crop_regression.csv',index = False)         
                                
                            
                                
                            else :
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average

                                
                                df.to_csv('crop_regression.csv',index = False)            

                    
                    if  first_year == parameters.get("exeptions").get(code).get("begin") and not last_year == parameters.get("exeptions").get(code).get("end") and (last_year-first_year)>=5:
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin")+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(parameters.get("exeptions").get(code).get("begin")+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False))])
                            
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)

                            if  np.sign(model.coef_) == np.sign(model2.coef_) :
                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin"))]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin")+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(parameters.get("exeptions").get(code).get("begin")+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([parameters.get("exeptions").get(code).get("begin")+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                        
                                df.to_csv('crop_regression.csv',index = False)         
                                

                                
                                
                            else :
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                
                                df.to_csv('crop_regression.csv',index = False)            
 
                                                                            
                    
                    if not first_year == parameters.get("exeptions").get(code).get("begin") and not last_year == parameters.get("exeptions").get(code).get("end") and (last_year-first_year)>=5:
                        if not df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]].isnull().values.all():
                            x = np.array([1, 2, 3]).reshape((-1, 1))
                            begin = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(first_year+2)]]).to_string(index=False,header=False))])
                            end = np.array([float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-1)]]).to_string(index=False,header=False)), float((df.loc[(df['ISO3']==code)&(df['Item Code']==item),["Y" + str(last_year-2)]]).to_string(index=False,header=False))])
                            model = LinearRegression().fit(x, begin) 
                            model2 = LinearRegression().fit(x, end)
                            if  np.sign(model.coef_) == np.sign(model2.coef_) :

                                value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year)]]
                                value1=float(value1.to_string(index=False, header=False))
                                value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+1)]]
                                value2=float(value2.to_string(index=False, header=False))
                                value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(first_year+2)]]
                                value3=float(value3.to_string(index=False, header=False))
                                
                                value4 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year)]]
                                value4=float(value4.to_string(index=False, header=False))
                                value5 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-1)]]
                                value5=float(value5.to_string(index=False, header=False))
                                value6 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(last_year-2)]]
                                value6=float(value6.to_string(index=False, header=False))
                                
                                average=(value1+value2+value3)/3
                                average2=(value4+value5+value6)/3
                                x = np.array([first_year+1, last_year-1]).reshape((-1, 1))
                                linear = np.array([average, average2])
                                model3=LinearRegression().fit(x, linear)
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value              
                                df.to_csv('crop_regression.csv',index = False) 
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    if model3.coef_ * years +model3.intercept_ >= 0 :
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=model3.coef_ * years +model3.intercept_
                                    else :
                                        previous_value = df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                        previous_value = float(previous_value.to_string(index=False, header=False))
                                        df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]= previous_value                                   
                                df.to_csv('crop_regression.csv',index = False) 
                                

                                
                            else :
                                for years in range(first_year-1,parameters.get("exeptions").get(code).get("begin")-1,-1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years+3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('crop_regression.csv',index = False) 
                                
                                for years in range(last_year+1,parameters.get("exeptions").get(code).get("end")+1):
                                    value1 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-1)]]
                                    value1=float(value1.to_string(index=False, header=False))
                                    value2 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-2)]]
                                    value2=float(value2.to_string(index=False, header=False))
                                    value3 =df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years-3)]]
                                    value3=float(value3.to_string(index=False, header=False))
                                    average=(value1+value2+value3)/3
                                    df.loc[(df['Item Code']==item)&(df['ISO3']==code),["Y" + str(years)]]=average
                                df.to_csv('crop_regression.csv',index = False) 


                    
    return df